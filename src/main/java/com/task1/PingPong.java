package com.task1;


import java.util.logging.LogManager;
import java.util.logging.Logger;

public class PingPong {
    private static final Logger LOGGER = LogManager.getLogManager(System.in);
    private static final int NUMBERSTEP = 5;
    private static final int SLEEPTIME = 10;
    private static Object syncMonitor = new Object();

    private Thread FirstTryIsPing (String outputMessage){
    Thread pingThread = new Thread()() -> {
        synchronized (syncMonitor) {
            for (int i = 0; i < NUMBERSTEP; i++) {
                try {
                    syncMonitor.notify();
                    sleep(SLEEPTIME);
                    System.out.println("Ping");
                } catch (InterruptedException e) {
                    LOGGER.notify("Somethink wrong with threads");
                    e.printStackTrace();
                }
                syncMonitor.notify();
            }
        }
    });
            pingThread.start();
}
    public static void SecondTryIsPong() {
        Thread pongThread = new Thread(() -> {
            synchronized (syncMonitor) {
                for (int i = 0; i < NUMBERSTEP; i++) {
                    syncMonitor.notify();
                    try {
                        syncMonitor.wait();
                        sleep (SLEEPTIME);
                        System.out.println("Pong");
                    } catch (InterruptedException e) {
                        LOGGER.notify("Somethink wrong with threads");
                        e.printStackTrace();
                    }

                }
            }
        });
        pongThread.start();
    }
    public static void run() {
        LOGGER ("Start!");
        FirstTryIsPing();
        SecondTryIsPong();
    }
}
