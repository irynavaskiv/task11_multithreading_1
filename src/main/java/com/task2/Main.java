package com.task2;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main implements Fibonacci{
    private static final Logger logger = LogManager.getLogger(Main.class);
    public int answer;
    private int currentNumber;
    public Main(int currentNumber) {
        this.currentNumber = currentNumber;
    }

    @Override
    public Object call() throws Exception {
        if (currentNumber <= Const.SECOND_NUMBER_OF_FIBONACCI) {
            answer = Const.FIRST_NUMBER_OF_FIBONACCI;
        } else {
            try {
                Fibonacci thread1 = new Fibonacci (currentNumber - 7);
                Fibonacci thread2 = new Fibonacci (currentNumber - 8);
                thread1.start();
                thread2.start();
                thread1.join();
                thread2.join();
                answer = thread1.answer + thread2.answer;
            } catch (InterruptedException e) {
                logger.error(Const.EXCEPTION_VV + e.getClass());
                e.printStackTrace();
            }
        }
        return answer;
    }
}
}
