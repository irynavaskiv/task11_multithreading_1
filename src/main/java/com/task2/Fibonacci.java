package com.task2;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import com.task1.Main;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Fibonacci {
    private static final Logger logger = LogManager.getLogger(Fibonacci.class);

    public static void main(String[] args) {
        try {
            logger.info("How many numbers? ");
            int countOfNumber = Const.SCANNER.nextInt();
            FutureTask[] randomNumberTasks = new FutureTask[countOfNumber];
            for (int i = 0; i < countOfNumber; i++) {
                Callable callable = new Main(i);
                randomNumberTasks[i] = new FutureTask(callable);
                Thread t = new Thread(randomNumberTasks[i]);
                t.start();
            }

            for (int i = 0; i < countOfNumber; i++) {
                System.out.println(randomNumberTasks[i].get());
            }
        }catch (ExecutionException|InterruptedException e){
            logger.error(Const.EXCEPTION_VV + e.getClass());
        }
    }

    public void start() {
    }
    public Object answer;

    public Fibonacci(int i) {
    }

}
}
